plugins {
    alias(libs.plugins.convention.android.application)
    alias(libs.plugins.convention.android.hilt)
    alias(libs.plugins.convention.android.navigation)
}

android {
    namespace = "com.bs23test.app"

    defaultConfig {
        applicationId = "com.bs23test.app"
        versionCode = 1
        versionName = "1.0"
    }

    buildTypes {
        release {
            signingConfig = signingConfigs.getByName("debug")
            isMinifyEnabled = true
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }
    buildFeatures {
        buildConfig = true
        viewBinding = true
    }

}

dependencies {

    with(projects) {
        implementation(common)
        implementation(core.data)
        implementation(core.network)
        implementation(core.di)
        implementation(core.ui)
        implementation(core.designSystem)
        implementation(core.domain)
        implementation(core.entity)

        implementation(feature.list)
        implementation(feature.detail)
    }

    with(libs) {
        implementation(androidx.core.ktx)
        implementation(androidx.appcompat)
        implementation(google.android.material)
        implementation(google.code.gson)
        implementation(androidx.activity.ktx)
        implementation(jakewharton.timber)
    }
}