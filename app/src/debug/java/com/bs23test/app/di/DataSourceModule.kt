package com.bs23test.app.di

import com.bs23test.network.datasource.BookDataSource
import com.bs23test.network.datasource.FakeBookDataSourceImpl
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface DataSourceModule {
    @Binds
    @Singleton
    fun bindBookDataSource(bookDataSource: FakeBookDataSourceImpl): BookDataSource

}