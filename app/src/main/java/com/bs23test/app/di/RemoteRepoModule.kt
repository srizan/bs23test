package com.bs23test.app.di

import com.bs23test.data.repository.remote.BookRepositoryImpl
import com.bs23test.domain.repository.BookRepository
import dagger.Binds
import dagger.Module
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
interface RemoteRepoModule {
    @Binds
    @Singleton
    fun bindBookRepository(bookRepository: BookRepositoryImpl): BookRepository
}