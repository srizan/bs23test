package com.bs23test.domain.base

import kotlinx.coroutines.flow.Flow

interface BaseUseCase

interface ApiUseCase<Params, Type> : BaseUseCase {
    suspend operator fun invoke(params: Params): Flow<ApiResult<Type>>
}

interface RoomSuspendableUseCase<Params, ReturnType> : BaseUseCase {
    suspend operator fun invoke(params: Params) : ReturnType
}

interface RoomCollectableUseCase<Params, ReturnType> : BaseUseCase {
    operator fun invoke(params: Params): Flow<ReturnType>
}


/**
 * Room Use Case
 * */

interface RoomUseCase<Params,ReturnType> : BaseUseCase {
    suspend fun execute(params: Params) : ReturnType
}

interface RoomVoidUseCase<Params> : BaseUseCase {
    suspend fun execute(params: Params)
}

interface RoomUseCaseNonParams<ReturnType> : BaseUseCase {
    suspend fun execute(): ReturnType
}



