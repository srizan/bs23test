package com.bs23test.domain.repository

import com.bs23test.domain.base.ApiResult
import com.bs23test.entity.BookListEntity
import kotlinx.coroutines.flow.Flow

interface BookRepository {
    suspend fun fetchBookList(): Flow<ApiResult<BookListEntity>>
}