plugins {
    alias(libs.plugins.convention.android.library)
}
android {
    namespace = "com.bs23test.domain"
}
dependencies{
    implementation(projects.core.entity)
    implementation(libs.javax.inject)
    implementation(libs.kotlinx.coroutines.core)
}