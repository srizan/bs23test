plugins {
    alias(libs.plugins.convention.android.library)
    alias(libs.plugins.convention.android.hilt)
}

android {
    namespace = "com.bs23test.di"
}

dependencies {
    implementation(libs.google.code.gson)
}