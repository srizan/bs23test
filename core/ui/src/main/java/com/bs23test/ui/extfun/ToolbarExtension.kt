package com.bs23test.ui.extfun

import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.Toolbar
import androidx.core.content.ContextCompat

fun AppCompatActivity.toolBarWithoutBackBtn(toolbar: Toolbar, title:String){
    setSupportActionBar(toolbar)
    supportActionBar?.title = title
}

fun AppCompatActivity.toolBarSubTitleWithoutBackBtn(toolbar: Toolbar,title:String,subTitle:String){
    setSupportActionBar(toolbar)
    supportActionBar?.title = title
    supportActionBar?.subtitle = subTitle
}

fun AppCompatActivity.toolBarSubTitleWithBackBtn(toolbar: Toolbar,title:String,subTitle:String){
    setSupportActionBar(toolbar)
    supportActionBar?.title = title
    supportActionBar?.subtitle = subTitle
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
}

fun AppCompatActivity.toolBarWithBackBtn(toolbar: Toolbar,title:String){
    setSupportActionBar(toolbar)
    supportActionBar?.title = title
    supportActionBar?.setDisplayHomeAsUpEnabled(true)
}


fun Toolbar.setNavigationIconColor(color: Int) = navigationIcon?.mutate()?.let {
    it.setTint(ContextCompat.getColor(this.context,color))
    this.navigationIcon = it
}


