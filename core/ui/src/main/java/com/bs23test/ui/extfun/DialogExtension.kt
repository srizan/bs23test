package com.bs23test.ui.extfun

import android.app.Activity
import android.content.Context
import android.view.View
import androidx.appcompat.app.AlertDialog
import androidx.core.content.ContextCompat


/**
 * @param view R.layout.your_custom_layout
 * @param title Dialog title (Avoid using it if you use custom view)
 * @param message Dialog message (Avoid using it if you use custom view)
 * @param positiveButtonText
 * @param positiveButtonTextColor R.color.your_color
 * @param positiveButtonBackgroundColor R.color.your_color
 * @param negativeButtonTextColor R.color.your_color
 * @param negativeButtonBackgroundColor R.color.your_color
 * @author Srizan
 * */
    fun Context.showAlertDialog(
    view: View? = null,
    title: String? = null,
    message: String? = null,
    positiveButtonText: String? = null,
    positiveButtonTextColor: Int = android.R.color.black,
    positiveButtonBackgroundColor: Int = android.R.color.transparent,

    negativeButtonText: String? = null,
    negativeButtonTextColor: Int = android.R.color.black,
    negativeButtonBackgroundColor: Int = android.R.color.transparent,

    cancelable: Boolean = true,
    positiveButtonCallback: (() -> Unit)? = null,
    negativeButtonCallback: (() -> Unit)? = null,
): AlertDialog {
    val builder = AlertDialog.Builder(this)
    positiveButtonText?.let {
        builder.setPositiveButton(it) { dialog, _ ->
            dialog.dismiss()
            positiveButtonCallback?.invoke()
        }
    }
    negativeButtonText?.let {
        builder.setNegativeButton(it) { dialog, _ ->
            dialog.dismiss()
            negativeButtonCallback?.invoke()
        }
    }
    view?.let(builder::setView)
    title?.let(builder::setTitle)
    message?.let(builder::setMessage)
    val dialog = builder
        .setCancelable(cancelable)
        .create()
    dialog.setOnShowListener {
        positiveButtonText?.let {
            val positiveButton = dialog.getButton(AlertDialog.BUTTON_POSITIVE)
            positiveButton.setTextColor(ContextCompat.getColor(this, positiveButtonTextColor))
            positiveButton.setBackgroundColor(ContextCompat.getColor(this, positiveButtonBackgroundColor))
        }
        negativeButtonText?.let {
            val negativeButton = dialog.getButton(AlertDialog.BUTTON_NEGATIVE)
            negativeButton.setTextColor(ContextCompat.getColor(this, negativeButtonTextColor))
            negativeButton.setBackgroundColor(ContextCompat.getColor(this, negativeButtonBackgroundColor))
        }
    }
    dialog.show()
    return dialog
}



fun Activity.showViewAlertDialogCustomTheme(
    view: View,
    theme: Int,
    cancelable: Boolean
): AlertDialog =
    AlertDialog.Builder(this, theme)
        .setView(view)
        .setCancelable(cancelable)
        .setTitle(title)
        .show()




