package com.bs23test.ui.extfun

import android.net.Uri
import androidx.annotation.IdRes
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleEventObserver
import androidx.lifecycle.MutableLiveData
import androidx.navigation.NavDeepLinkRequest
import androidx.navigation.fragment.findNavController
import androidx.navigation.navOptions
import com.bs23test.designsystem.R

fun Fragment.popBack(){
    findNavController().popBackStack()
}

fun <T> Fragment.navigationBackStackResultLiveData(key: String = "key"): MutableLiveData<T>? {

    viewLifecycleOwner.lifecycle.addObserver(LifecycleEventObserver { _, event ->
        if (event == Lifecycle.Event.ON_DESTROY) {
            findNavController().previousBackStackEntry?.savedStateHandle?.remove<T>(key)
        }
    })

    return findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData(key)
}

fun <T> Fragment.setNavigationBackStackResult(key: String, result: T) {
    findNavController().previousBackStackEntry?.savedStateHandle?.set(key, result)
}

fun <T> Fragment.setBackStackResultOnDestination(
    @IdRes destinationId: Int,
    key: String,
    result: T
) {
    findNavController().getBackStackEntry(destinationId).savedStateHandle[key] = result
}


fun Fragment.navigate(
    uri: Uri,
    popupToId:Int? = null,
    popupToInclusive:Boolean = false
){
    findNavController().navigate(
        NavDeepLinkRequest.Builder.fromUri(uri).build(),
        navOptions = navOptions {
            anim {
                enter = R.anim.slide_in_right
                exit = R.anim.slide_out_left
                popEnter = R.anim.slide_in_left
                popExit = R.anim.slide_out_right
            }
            popupToId?.let {
                popUpTo(popupToId){
                    inclusive = popupToInclusive
                }
            }
        }
    )
}