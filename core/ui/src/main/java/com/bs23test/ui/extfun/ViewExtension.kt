package com.bs23test.ui.extfun

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.net.Uri
import android.os.SystemClock
import android.text.TextUtils
import android.util.Patterns
import android.view.View
import android.view.Window
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.AutoCompleteTextView
import android.widget.EditText
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.core.view.isVisible
import com.google.android.material.textfield.TextInputEditText
import com.google.android.material.textfield.TextInputLayout

fun EditText.getTextFromEt(): String = this.text.toString().trim()

fun EditText.setTextWithCursorInTheEnd(text: String) {
    this.setText(text)
    this.setSelection(text.length)
}

fun EditText.getTextAsInteger(): Int {
    return try {
        this.text.toString().trim().toInt()
    } catch (ex: Exception) {
        0
    }
}


fun AutoCompleteTextView.getTextFromAt(): String = this.text.toString().trim()


fun EditText.isEmptyInput(errorMessage: String): Boolean {
    val input = this.text.toString().trim()
    if (input.isEmpty()) this.error = errorMessage
    return input.isEmpty()
}

fun TextView.isEmpty(text: String, errorText: String, errorTV: TextView): Boolean {
    errorTV.isVisible = text.isBlank()
    errorTV.text = errorText
    return text.isBlank()
}

fun TextInputEditText.checkErrorToText(
    textInputLayout: TextInputLayout,
    errorMessage: String
): String {
    val text = this.text.toString()
    if (text.isEmpty()) textInputLayout.error = errorMessage
    return text
}

fun TextInputLayout.checkErrorToText(
    errorMessage: String
): String {
    val text = this.editText?.text.toString()
    if (text.isEmpty()) error = errorMessage
    return text
}

fun View.hideKeyboard() {
    val inputMethodManager =
        context.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
    inputMethodManager.hideSoftInputFromWindow(windowToken, 0)
}

fun EditText.showSoftKeyboard(window: Window?) {
    window?.clearFlags(WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE or WindowManager.LayoutParams.FLAG_ALT_FOCUSABLE_IM)
    window?.setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE)
    if (this.requestFocus()) {
        val imm = ContextCompat.getSystemService(this.context, InputMethodManager::class.java)
        imm?.showSoftInput(this, InputMethodManager.SHOW_IMPLICIT)
    }
}

fun Activity.startPhoneCallDial(phoneNumber: String?) {
    phoneNumber?.let {
        val intent = Intent(Intent.ACTION_DIAL)
        intent.data = Uri.parse("tel:$it")
        startActivity(intent)
    }
}

var lastClickTime: Long = 0
fun View.clickWithDebounce(debounceTime: Long = 800L, action: () -> Unit) {
    this.setOnClickListener(object : View.OnClickListener {
        override fun onClick(v: View) {
            if (SystemClock.elapsedRealtime() - lastClickTime < debounceTime) return
            else action()
            lastClickTime = SystemClock.elapsedRealtime()
        }
    })
}

inline infix fun View.onClick(crossinline action: () -> Unit) {
    this.setOnClickListener { action.invoke() }
}


fun EditText.isEmailValid(email: CharSequence): Boolean {
    return !TextUtils.isEmpty(email) && Patterns.EMAIL_ADDRESS.matcher(email).matches()
}


