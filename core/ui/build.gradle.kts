plugins {
    alias(libs.plugins.convention.android.library)
}

android {
    namespace = "com.bs23test.ui"
}

dependencies {
    implementation(projects.common)
    implementation(projects.core.designSystem)

    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.androidx.navigation.fragment.ktx)
    implementation(libs.google.android.material)

}