plugins {
    alias(libs.plugins.convention.android.library)
}

android {
    namespace = "com.bs23test.designsystem"
}

dependencies {
    implementation(libs.androidx.core.ktx)
    implementation(libs.androidx.appcompat)
    implementation(libs.google.android.material)
}