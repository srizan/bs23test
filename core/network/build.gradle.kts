plugins {
    alias(libs.plugins.convention.android.library)
    alias(libs.plugins.convention.android.hilt)
}
android {
    namespace = "com.bs23test.network"
}

dependencies {
    implementation(projects.core.di)
    implementation(projects.core.domain)
    implementation(projects.core.entity)
    implementation(libs.jakewharton.timber)
    implementation(libs.bundles.network.essential.bundle)
}