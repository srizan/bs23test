package com.bs23test.network.datasource

import com.bs23test.network.wrapper.NetworkResourceDownLoader
import com.bs23test.domain.base.ApiResult
import com.bs23test.entity.BookListEntity
import com.bs23test.network.mapper.BookListMapper
import com.bs23test.network.mapper.mapFromApiResponse
import com.bs23test.network.service.BookApiService
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookDataSourceImpl @Inject constructor(
    private val networkResourceDownLoader: NetworkResourceDownLoader,
    private val bookApiService: BookApiService,
    private val bookListMapper: BookListMapper,

    ) : BookDataSource {
    override suspend fun getBooks(): Flow<ApiResult<BookListEntity>> {
        return mapFromApiResponse(
            apiResult = networkResourceDownLoader.downloadData {
                bookApiService.getBooks()
            },
            mapper = bookListMapper
        )
    }
}