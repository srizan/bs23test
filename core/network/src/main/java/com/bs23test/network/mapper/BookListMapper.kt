package com.bs23test.network.mapper

import com.bs23test.entity.BookEntity
import com.bs23test.entity.BookListEntity
import com.bs23test.network.response.BookListResponse
import com.bs23test.network.response.BookResponse
import javax.inject.Inject

class BookListMapper @Inject constructor() : Mapper<BookListResponse, BookListEntity> {
    override fun mapFromApiResponse(type: BookListResponse): BookListEntity {
        return BookListEntity(
            status = type.status ?: 0,
            message = type.message ?: "",
            data = type.data?.map { it.asEntity() } ?: emptyList(),
        )
    }
}

fun BookResponse.asEntity() = BookEntity(
    id = this.id ?: 0,
    name = this.name ?: "",
    url = this.url ?: "",
    image = this.image ?: "",
)