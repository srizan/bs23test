package com.bs23test.network.datasource

import com.bs23test.domain.base.ApiResult
import com.bs23test.entity.BookListEntity
import kotlinx.coroutines.flow.Flow

interface BookDataSource {
    suspend fun getBooks(): Flow<ApiResult<BookListEntity>>
}