package com.bs23test.network.datasource

import com.bs23test.domain.base.ApiResult
import com.bs23test.entity.BookListEntity
import com.bs23test.network.mapper.asEntity
import com.bs23test.network.response.BookListResponse
import com.bs23test.network.util.JsonReader
import com.google.gson.Gson
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.flow
import javax.inject.Inject

class FakeBookDataSourceImpl @Inject constructor(
    private val jsonReader: JsonReader,
    private val gson: Gson
) : BookDataSource {
    override suspend fun getBooks(): Flow<ApiResult<BookListEntity>> {
        return flow {
            emit(ApiResult.Loading(true))

            val jsonString = jsonReader.getJsonFromAssets("book_list.json")

            val response = gson.fromJson(jsonString, BookListResponse::class.java)

            val resEntity = BookListEntity(
                status = response.status ?: 0,
                message = response.message ?: "",
                data = response.data?.map {
                    it.asEntity()
                } ?: emptyList()
            )
            emit(ApiResult.Loading(false))
            emit(ApiResult.Success(resEntity))
        }
    }
}