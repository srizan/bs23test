package com.bs23test.network.di

import com.bs23test.network.service.BookApiService
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import retrofit2.Retrofit
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
object ApiServiceModule {

    @Provides
    @Singleton
    fun provideGithubApiServices(
        retrofitBuilder: Retrofit.Builder
    ): BookApiService {

        val baseUrl: String = "https://example.com/"
        val retrofit = retrofitBuilder
            .baseUrl(baseUrl)
            .build()
        return retrofit.create(BookApiService::class.java)
    }

}