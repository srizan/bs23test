package com.bs23test.network.response

import java.io.Serializable

data class BookListResponse(
    val status: Int?,
    val message: String?,
    val data: List<BookResponse>?
) : Serializable

data class BookResponse(
    val id: Int?,
    val name: String?,
    val image: String?,
    val url: String?
) : Serializable