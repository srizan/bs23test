package com.bs23test.network.service

import com.bs23test.network.response.BookListResponse
import retrofit2.Response
import retrofit2.http.GET

interface BookApiService {
    @GET("/api/book-list")
    suspend fun getBooks(): Response<BookListResponse>
}