package com.bs23test.network.util;

import android.content.Context
import dagger.hilt.android.qualifiers.ApplicationContext
import java.io.IOException
import javax.inject.Inject


class JsonReader @Inject constructor(
    @ApplicationContext
    private val applicationContext: Context
) {
    fun getJsonFromAssets(fileName: String?): String? {
        val jsonString: String
        try {
            val stream = applicationContext.assets.open(fileName!!)

            val size = stream.available()
            val buffer = ByteArray(size)
            stream.read(buffer)
            stream.close()

            jsonString = String(buffer, charset("UTF-8"))
        } catch (e: IOException) {
            e.printStackTrace()
            return null
        }
        return jsonString
    }
}