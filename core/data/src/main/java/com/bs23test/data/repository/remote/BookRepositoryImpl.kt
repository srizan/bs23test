package com.bs23test.data.repository.remote

import com.bs23test.domain.base.ApiResult
import com.bs23test.domain.repository.BookRepository
import com.bs23test.entity.BookListEntity
import com.bs23test.network.datasource.BookDataSource
import kotlinx.coroutines.flow.Flow
import javax.inject.Inject

class BookRepositoryImpl @Inject constructor(
    private val bookDataSource: BookDataSource
) : BookRepository {
    override suspend fun fetchBookList(): Flow<ApiResult<BookListEntity>> {
        return bookDataSource.getBooks()
    }
}