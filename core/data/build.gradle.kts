plugins {
    alias(libs.plugins.convention.android.library)
}

android {
    namespace = "com.bs23test.data"
}

dependencies {
    with(projects) {
        implementation(core.domain)
        implementation(core.entity)
        implementation(core.network)
        //implementation(core.database)
        //implementation(library.dateTime)
        //implementation(library.sharedpref)
    }

    with(libs) {
        implementation(kotlinx.coroutines.android)
        implementation(javax.inject)
        implementation(squareup.retrofit2.retrofit)
        implementation(google.code.gson)
    }
}