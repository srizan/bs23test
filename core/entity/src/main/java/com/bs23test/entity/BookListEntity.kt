package com.bs23test.entity

import java.io.Serializable

data class BookListEntity(
    val status: Int,
    val message: String,
    val data: List<BookEntity>
)

data class BookEntity(
    val id: Int,
    val name: String,
    val image: String,
    val url: String
) : Serializable