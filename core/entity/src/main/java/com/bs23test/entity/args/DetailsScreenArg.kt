package com.bs23test.entity.args


import com.bs23test.entity.BookEntity
import java.io.Serializable

data class DetailsScreenArg(
    val book: BookEntity
) : Serializable