pluginManagement {
    includeBuild("build-logic")
    repositories {
        google {
            content {
                includeGroupByRegex("com\\.android.*")
                includeGroupByRegex("com\\.google.*")
                includeGroupByRegex("androidx.*")
            }
        }
        mavenCentral()
        gradlePluginPortal()
    }
}
dependencyResolutionManagement {
    repositoriesMode.set(RepositoriesMode.FAIL_ON_PROJECT_REPOS)
    repositories {
        google()
        mavenCentral()
    }
}
enableFeaturePreview("TYPESAFE_PROJECT_ACCESSORS")

rootProject.name = "bs23-test"
include(":app")
include(":common")
include(":feature")

include(":core:entity")
include(":core:domain")
include(":core:network")

include(":core:data")
include(":core:ui")
include(":core:design-system")
include(":core:di")

include(":feature:list")
include(":feature:detail")



