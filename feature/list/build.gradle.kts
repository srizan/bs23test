plugins {
    alias(libs.plugins.convention.android.library.feature)
}

android {
    namespace = "com.bs23test.list"
}

dependencies {
    implementation(libs.androidx.recyclerview)
    implementation(libs.coil)
}