package com.bs23test.list

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.bs23test.domain.base.ApiResult
import com.bs23test.domain.repository.BookRepository
import com.bs23test.entity.BookEntity
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.flow.MutableStateFlow
import kotlinx.coroutines.flow.asStateFlow
import kotlinx.coroutines.launch
import javax.inject.Inject


sealed interface UiState {
    data class Success(val bookList: List<BookEntity>) : UiState
    data class Error(val message: String) : UiState
    data class Loading(val isLoading: Boolean) : UiState
}


@HiltViewModel
class ListViewModel @Inject constructor(
    private val bookRepository: BookRepository
) : ViewModel() {

    private val _state = MutableStateFlow<UiState>(UiState.Loading(false))
    val state = _state.asStateFlow()

    init {
        fetchBookList()
    }

    fun fetchBookList() {
        viewModelScope.launch {
            bookRepository.fetchBookList().collect { result ->
                when (result) {
                    is ApiResult.Error -> _state.value = UiState.Error(result.errorMessage)
                    is ApiResult.Loading -> _state.value = UiState.Loading(result.isLoading)
                    is ApiResult.Success -> _state.value = UiState.Success(result.data.data)
                }
            }
        }
    }
}