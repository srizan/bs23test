package com.bs23test.list

import android.os.Bundle
import androidx.core.net.toUri
import androidx.core.view.isVisible
import androidx.fragment.app.viewModels
import com.bs23test.common.base.ViewBindingFragment
import com.bs23test.common.extension.encode
import com.bs23test.entity.BookEntity
import com.bs23test.entity.args.DetailsScreenArg
import com.bs23test.list.adapter.BookListAdapter
import com.bs23test.list.databinding.FragmentListBinding
import com.bs23test.ui.extfun.navigate
import com.bs23test.ui.extfun.setUpVerticalRecyclerView
import com.bs23test.ui.utils.autoCleared
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject

@AndroidEntryPoint
class ListFragment : ViewBindingFragment<FragmentListBinding>() {

    @Inject
    lateinit var gson: Gson
    private val viewModel: ListViewModel by viewModels()
    private var bookListAdapter by autoCleared<BookListAdapter>()

    override fun inflateLayout() = FragmentListBinding.inflate(layoutInflater)

    override fun onViewCreated(savedInstanceState: Bundle?) {
        setupAdapter()
        observeUiState()
    }

    private fun observeUiState() {
        viewModel.state.collectFlowOnStarted { state ->
            when (state) {
                is UiState.Error -> showToastMessage(state.message)
                is UiState.Loading -> {
                    binding.progressbar.isVisible = state.isLoading
                    binding.list.isVisible = state.isLoading.not()
                }

                is UiState.Success -> bookListAdapter.submitList(state.bookList)
            }
        }
    }

    private fun setupAdapter() {
        bookListAdapter = BookListAdapter { navigateToDetailsFragment(it) }
        requireContext().setUpVerticalRecyclerView(binding.list, bookListAdapter)
    }


    private fun navigateToDetailsFragment(bookEntity: BookEntity) {
        val arg: String = gson.toJson(DetailsScreenArg(bookEntity)).encode()
        navigate("app://com.bs23test/details/args=$arg".toUri())
    }

}