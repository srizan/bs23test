package com.bs23test.list.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import coil.load
import com.bs23test.common.base.BaseListAdapter
import com.bs23test.entity.BookEntity
import com.bs23test.list.databinding.ItemBookBinding

class BookListAdapter(
    private val onItemClick: (BookEntity) -> Unit
) : BaseListAdapter<BookEntity, ItemBookBinding>(diffCallback = object :
    DiffUtil.ItemCallback<BookEntity>() {

    override fun areItemsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
        return oldItem == newItem
    }

    override fun areContentsTheSame(oldItem: BookEntity, newItem: BookEntity): Boolean {
        return oldItem == newItem
    }
}) {
    override fun createBinding(parent: ViewGroup) =
        ItemBookBinding.inflate(LayoutInflater.from(parent.context), parent, false)

    override fun bind(binding: ItemBookBinding, item: BookEntity, position: Int) {
        binding.root.setOnClickListener { onItemClick(item) }
        binding.name.text = item.name

        binding.image.load(item.image) {
            placeholder(com.bs23test.designsystem.R.drawable.ic_progress)
        }
    }
}