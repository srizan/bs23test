plugins {
    alias(libs.plugins.convention.android.library.feature)
}

android {
    namespace = "com.bs23test.detail"
}

dependencies {
    implementation(libs.coil)
}