package com.bs23test.detail

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import androidx.navigation.fragment.navArgs
import coil.load
import com.bs23test.common.base.ViewBindingFragment
import com.bs23test.common.extension.decode
import com.bs23test.detail.databinding.FragmentDetailBinding
import com.bs23test.entity.args.DetailsScreenArg
import com.bs23test.ui.extfun.popBack
import com.bs23test.ui.extfun.setNavigationIconColor
import com.google.gson.Gson
import dagger.hilt.android.AndroidEntryPoint
import javax.inject.Inject


@AndroidEntryPoint
class DetailFragment : ViewBindingFragment<FragmentDetailBinding>() {


    @Inject
    lateinit var gson: Gson

    private val _args by navArgs<DetailFragmentArgs>()
    private val args: DetailsScreenArg by lazy {
        gson.fromJson(_args.encodedJson.decode(), DetailsScreenArg::class.java)
    }

    override fun inflateLayout() = FragmentDetailBinding.inflate(layoutInflater)
    override fun onViewCreated(savedInstanceState: Bundle?) {
        binding.apply {
            setupToolbar()
            name.text = args.book.name
            image.load(args.book.image)
            url.text = "URL: " + args.book.url

            wiki.setOnClickListener {
                val intent = Intent(Intent.ACTION_VIEW)
                intent.setData(Uri.parse(args.book.url))
                startActivity(intent)
            }
        }
    }

    private fun FragmentDetailBinding.setupToolbar() {
        toolBar.apply {
            setNavigationIcon(com.bs23test.designsystem.R.drawable.ic_arrow_back)
            setNavigationIconColor(com.bs23test.designsystem.R.color.white)
            setNavigationOnClickListener { popBack() }
        }
    }
}