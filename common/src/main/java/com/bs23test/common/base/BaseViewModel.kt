package com.bs23test.common.base

import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.SharingStarted
import kotlinx.coroutines.flow.stateIn
import kotlinx.coroutines.launch

open class BaseViewModel() : ViewModel() {
    /**
     * @param job A suspend function that you want to launch in viewModelScope
     * @return A coroutine Job
     * */
    fun launchJob(job: suspend () -> Unit) = viewModelScope.launch { job.invoke() }

    /**
     * Convert a Cold flow into Hot State flow using stateIn operator
     * @param scope CoroutineScope
     * @param started SharingStarted.Eagerly policy to listen for updates even if there are no collectors.
     * @param initialValue Your Initial Value of State
     * @see <a href="https://kotlinlang.org/api/kotlinx.coroutines/kotlinx-coroutines-core/kotlinx.coroutines.flow/state-in.html">Flow<T>.stateIn</a>
     * @see <a href="https://medium.com/androiddevelopers/things-to-know-about-flows-sharein-and-statein-operators-20e6ccb2bc74">Things to know about Flow’s shareIn and stateIn operators</a>
     * @author Srizan
     * */
    fun <T> Flow<T>.initializeStateWith(
        initialValue: T,
        scope: CoroutineScope = viewModelScope,
        started: SharingStarted = SharingStarted.Eagerly
    ) = stateIn(
        scope = scope,
        started = started,
        initialValue = initialValue,
    )
}