package com.bs23test.common.base

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.MenuItem
import android.view.View
import android.view.WindowManager
import android.widget.Toast
import androidx.annotation.StringRes
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.lifecycleScope
import androidx.lifecycle.repeatOnLifecycle
import androidx.viewbinding.ViewBinding
import com.google.android.material.snackbar.Snackbar
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.launch

abstract class ViewBindingActivity<D : ViewBinding> : AppCompatActivity() {

    protected lateinit var binding: D
    protected abstract fun inflateLayout(): D
    protected abstract fun initializeView(savedInstanceState: Bundle?)
    private lateinit var toast: Toast

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = inflateLayout()
        setContentView(binding.root)
        initializeView(savedInstanceState)
    }

    protected fun showMessage(message: String?) {
        Snackbar.make(findViewById(android.R.id.content), "" + message, Snackbar.LENGTH_LONG).show()
    }

    protected fun showSnackBar(
        message: String?,
        @StringRes resId: Int? = null,
        duration: Int = Snackbar.LENGTH_LONG,
        action: (() -> Unit)? = null,
    ) {
        val snackBar =
            Snackbar.make(
                findViewById(android.R.id.content),
                "" + message,
                duration
            )
        resId?.let { snackBar.setAction(it) { action?.invoke() } }
        snackBar.show()
    }

    protected fun showToastMessage(message: String?) {
        if (this::toast.isInitialized) toast.cancel()
        toast = Toast.makeText(applicationContext, message, Toast.LENGTH_SHORT)
        toast.show()
    }

    protected fun showProgressBar(isLoading: Boolean, view: View) {
        if (isLoading) {
            view.visibility = View.VISIBLE
            window.setFlags(
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE,
                WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE
            )
        } else {
            view.visibility = View.GONE
            window.clearFlags(WindowManager.LayoutParams.FLAG_NOT_TOUCHABLE)
        }
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) finish()
        return super.onOptionsItemSelected(item)
    }


    @SuppressLint("UnsafeRepeatOnLifecycleDetector")
    protected inline infix fun <T> Flow<T>.collectFlowOnStarted(crossinline action: (T) -> Unit) {
        lifecycleScope.launch {
            repeatOnLifecycle(Lifecycle.State.STARTED) {
                collect {
                    action(it)
                }
            }
        }
    }
}