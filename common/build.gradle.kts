plugins {
    alias(libs.plugins.convention.android.library)
}
android {
    namespace = "com.bs23test.common"
    buildFeatures {
        viewBinding = true
    }
}
dependencies {
    implementation(libs.androidx.activity.ktx)
    implementation(libs.google.android.material)
    implementation(libs.androidx.lifecycle.viewmodel.ktx)
    implementation(libs.androidx.lifecycle.runtime.ktx)
    implementation(libs.kotlinx.coroutines.android)
}