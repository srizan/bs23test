package com.bs23test.buildlogic.plugin

import com.android.build.api.dsl.ApplicationExtension
import configureCompose
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

class AndroidApplicationComposeConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("convention.android.application")
            }
            configureCompose(extensions.getByType<ApplicationExtension>())
        }
    }
}