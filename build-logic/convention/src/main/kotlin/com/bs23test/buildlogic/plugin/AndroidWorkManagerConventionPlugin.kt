package com.bs23test.buildlogic.plugin

import com.bs23test.buildlogic.extension.libs
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.dependencies

class AndroidWorkManagerConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("convention.kotlin.kapt")
            }
            dependencies {
                add("implementation", libs.findLibrary("androidx.startup.runtime").get())
                add("implementation", libs.findLibrary("androidx.work.runtime.ktx").get())
                add("implementation", libs.findLibrary("androidx.hilt.work").get())
                add("kapt", libs.findLibrary("androidx.hilt.compiler").get())
            }
        }
    }
}
