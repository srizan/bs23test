package com.bs23test.buildlogic.plugin

import com.bs23test.buildlogic.extension.libs
import org.gradle.api.Plugin
import org.gradle.api.Project

class AndroidHiltConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("convention.kotlin.kapt")
                apply("com.google.dagger.hilt.android")
            }

            with(dependencies) {
                add("implementation", libs.findLibrary("dagger.hilt.android").get())
                add("kapt", libs.findLibrary("dagger.hilt.compiler").get())
            }
        }
    }
}