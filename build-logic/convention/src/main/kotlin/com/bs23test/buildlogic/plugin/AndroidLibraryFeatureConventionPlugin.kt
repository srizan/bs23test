package com.bs23test.buildlogic.plugin

import com.android.build.gradle.LibraryExtension
import com.bs23test.buildlogic.extension.libs
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.configure
import org.gradle.kotlin.dsl.dependencies

class AndroidLibraryFeatureConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            pluginManager.apply {
                apply("convention.android.library")
                apply("convention.android.navigation")
                apply("convention.android.hilt")
            }
            extensions.configure<LibraryExtension> {
                buildFeatures {
                    viewBinding = true
                }
            }
            dependencies {
                add("implementation", project(":common"))
                add("implementation", project(":core:domain"))
                add("implementation", project(":core:entity"))
                add("implementation", project(":core:design-system"))
                add("implementation", project(":core:di"))
                add("implementation", project(":core:ui"))

                add("implementation", libs.findLibrary("androidx.core.ktx").get())
                add("implementation", libs.findLibrary("androidx.fragment.ktx").get())
                add("implementation", libs.findLibrary("androidx.appcompat").get())
                add("implementation", libs.findLibrary("androidx.constraintlayout").get())
                add("implementation", libs.findLibrary("google.android.material").get())
                add("implementation", libs.findLibrary("google.code.gson").get())

                add("implementation", libs.findLibrary("androidx-lifecycle-runtime-ktx").get())
                add("implementation", libs.findLibrary("androidx-lifecycle-viewmodel-ktx").get())
                add("implementation", libs.findLibrary("androidx-lifecycle-viewmodel-savedstate").get())

                add("implementation", libs.findLibrary("kotlinx.coroutines.android").get())
            }
        }
    }
}