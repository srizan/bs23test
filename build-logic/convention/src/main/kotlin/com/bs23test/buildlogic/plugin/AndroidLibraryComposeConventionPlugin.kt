package com.bs23test.buildlogic.plugin

import com.android.build.gradle.LibraryExtension
import configureCompose
import org.gradle.api.Plugin
import org.gradle.api.Project
import org.gradle.kotlin.dsl.getByType

class AndroidLibraryComposeConventionPlugin : Plugin<Project> {
    override fun apply(target: Project) {
        with(target) {
            with(pluginManager) {
                apply("convention.android.library")
            }
            configureCompose(extensions.getByType<LibraryExtension>())
        }
    }
}