package com.bs23test.buildlogic.extension

import com.android.build.api.dsl.CommonExtension
import com.bs23test.buildlogic.constant.COMPILE_SDK
import com.bs23test.buildlogic.constant.MIN_SDK
import org.gradle.api.JavaVersion
import org.gradle.api.Project
import org.gradle.api.plugins.ExtensionAware
import org.gradle.kotlin.dsl.dependencies
import org.jetbrains.kotlin.gradle.dsl.KotlinJvmOptions

internal fun Project.configureKotlinAndroid(
    commonExtension: CommonExtension<*, *, *, *, *, *>,
) {
    commonExtension.apply {
        compileSdk = COMPILE_SDK

        defaultConfig {
            minSdk = MIN_SDK
        }
        compileOptions {
            sourceCompatibility = JavaVersion.VERSION_1_8
            targetCompatibility = JavaVersion.VERSION_1_8
        }
        kotlinOptions {
            jvmTarget = "1.8"
        }
    }

    dependencies {
        add("testImplementation", libs.findLibrary("test-junit").get())
        add("androidTestImplementation", libs.findLibrary("test-androidx-junit").get())
        add("androidTestImplementation", libs.findLibrary("test-androidx-espresso-core").get())
    }
}

fun CommonExtension<*, *, *, *, *, *>.kotlinOptions(block: KotlinJvmOptions.() -> Unit) {
    (this as ExtensionAware).extensions.configure("kotlinOptions", block)
}