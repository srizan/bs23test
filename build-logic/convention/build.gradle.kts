import org.jetbrains.kotlin.gradle.tasks.KotlinCompile

plugins {
    `kotlin-dsl`
}

// Configure the build-logic plugins to target JDK 17
// This matches the JDK used to build the project, and is not related to what is running on device.
java {
    sourceCompatibility = JavaVersion.VERSION_17
    targetCompatibility = JavaVersion.VERSION_17
}
tasks.withType<KotlinCompile>().configureEach {
    kotlinOptions {
        jvmTarget = JavaVersion.VERSION_17.toString()
    }
}

dependencies {
    compileOnly(libs.android.gradlePlugin)
    compileOnly(libs.kotlin.gradlePlugin)
    compileOnly(libs.ksp.gradlePlugin)
}

gradlePlugin {

    plugins {
        val packageName = "com.bs23test.buildlogic.plugin"
        register("androidApplication") {
            id = "convention.android.application"
            implementationClass = "$packageName.AndroidApplicationConventionPlugin"
        }

        register("androidLibrary") {
            id = "convention.android.library"
            implementationClass = "$packageName.AndroidLibraryConventionPlugin"
        }

        register("androidLibraryFeature") {
            id = "convention.android.library.feature"
            implementationClass = "$packageName.AndroidLibraryFeatureConventionPlugin"
        }

        register("kapt") {
            id = "convention.kotlin.kapt"
            implementationClass = "$packageName.KaptConventionPlugin"
        }

        register("hilt") {
            id = "convention.android.hilt"
            implementationClass = "$packageName.AndroidHiltConventionPlugin"
        }

        register("room") {
            id = "convention.android.room"
            implementationClass = "$packageName.AndroidRoomConventionPlugin"
        }

        register("workmanager") {
            id = "convention.android.workmanager"
            implementationClass = "$packageName.AndroidWorkManagerConventionPlugin"
        }
        register("navigation") {
            id = "convention.android.navigation"
            implementationClass = "$packageName.AndroidNavigationConventionPlugin"
        }
    }
    /*    plugins {
            register("androidFirebase") {
                id = "iamkamrul.android.firebase"
                implementationClass = "AndroidApplicationFirebaseConventionPlugin"
            }



            register("jvmLibrary") {
                id = "iamkamrul.jvm.library"
                implementationClass = "JvmLibraryConventionPlugin"
            }


        }*/
}
